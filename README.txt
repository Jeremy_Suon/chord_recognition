Hello, this is a small python program to help build chord recognition through ear training exercises.

If having problems, stop, get some help. You can contact me at Jeremy.Suon@gmail.com

Things you'll need to run this program:

Install Python version 3.0 or higher
- Note, while installing python, there're options in the installation guide to set 
  global variables and to install pip. Check both these boxes.

Run Command Line Terminal as Administrative 
- To do this, press windows key, type in CMD, 
  right click Command Prompt, and click Run as Administrative

- In terminal, type "python3 -m pip install -U pygame --user"
  - This should show a loading bar while installing, as indication that it's actually doing it

Open IDLE (Python version x.x), you can press the windows key and type in IDLE to find it

Click File -> Open -> Navigate to Ear_Training.py

Press F5 to run

Enjoy.