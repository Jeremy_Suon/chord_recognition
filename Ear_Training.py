import os
import random
import time
from datetime import datetime
from pygame import mixer

## global variables ##
high_CQR = 0
high_SSR = 0
high_CPR = 0


## function objective: outputs introductory statement when program is first loaded ##
def intro():
    print("Welcome to Ear Training: Chord Recognition")
    print("You will have 3 training options to select from: ")
    print("  Chord Quality Recognition(CQR)")
    print("  Single Sound Recognition(SSR)")
    print("  Chord Progression Recognition(CPR)")

## function objective: presents the default list of options for the program ##
def options():
    print("\n\n-----EAR TRAINING: CHORD RECOGNITION-----")
    print("\nList of options:")
    print("1. Chord Quality Recognition(CQR)")
    print("2. Single Sound Recognition(SSR)")
    print("3. Chord Progression Recognition(CPR)")
    print("4. Help (explanation of what each training option contains)")
    print("5. Exit")

## function objective: provide functionality for training program for Chord Quality Recognition ##
def CQR():

    ## instantiate variables ##
    arr = os.listdir('Chords')
    arr_majmin = []
    local_CQR = 0
    global high_CQR

    ## get the major/minor chord variants within the Chord directory folder ##
    for chord in arr:
        letter = chord[0]
        major = letter + "major.mp3"
        minor = letter + "minor.mp3"
        if((major in arr) and (minor in arr) and (major not in arr_majmin)):
            arr_majmin.append(major)
            arr_majmin.append(minor)
    flag = 1

    ## main function loop ##
    while(flag == 1):
        replay = 1
        print("\n\n-----CHORD QUALITY RECOGNITION(CQR)-----")
        print("\nYour highest CQR record is: " + str(high_CQR))
        print("Your current CQR record is: " + str(local_CQR))
        print("List of possible chords:")
        for chord in arr_majmin:
            print("  " + chord[:-4])
        rand = random.randint(0, len(arr_majmin) - 1)
        mixer.init()
        mixer.music.load('Chords/'+arr_majmin[rand])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)
        print("The chord being played is: " + arr_majmin[rand][0])

        ## option processing ##
        while(replay == 1):
            replay = 0
            option = input("Was the chord played in major or minor? (To exit, type 'EXIT', to replay, type 'REPLAY'): ")
            if option.upper() == arr_majmin[rand][1:-4].upper():
                print("Congrats! You got it right!")
                local_CQR += 1
                if local_CQR > high_CQR:
                    high_CQR = local_CQR
                    print("CQR record Updated! Highest CQR record is now: " + str(high_CQR))
            elif option.upper() == "EXIT":
                print("Thank you for using Chord Quality Recognition(CQR).")
                flag = 0
            elif option.upper() == "REPLAY":
                replay = 1
                print("")
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
            else:
                print("Your answer was incorrect. Resetting local CQR record.")
                print("The presented chord was played in: " + arr_majmin[rand][1:-4])
                local_CQR = 0

## function objective: provide functionality for training program for Single Sound Recognition ##
def SSR():

    ## instantiate variables ##
    arr = os.listdir('Chords')
    flag = 1
    local_SSR = 0
    global high_SSR

    ## main function loop ##
    while(flag == 1):
        replay = 1
        print("\n\n-----SINGLE SOUND RECOGNITION(SSR)-----")
        print("\nYour highest SSR record is: " + str(high_SSR))
        print("Your current SSR record is: " + str(local_SSR))
        print("List of possible chords:")
        for chord in arr:
            print("  " + chord[:-4])
        rand = random.randint(0, len(arr) - 1)
        mixer.init()
        mixer.music.load('Chords/'+arr[rand])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)

        ## option processing ##
        while(replay == 1):
            replay = 0
            option = input("Which chord was played? (To exit, type 'EXIT', to replay, type 'REPLAY'): ")
            if option.upper() == arr[rand][:-4].upper():
                print("Congrats! You got it right!")
                local_SSR += 1
                if local_SSR > high_SSR:
                    high_SSR = local_SSR
                    print("SSR record Updated! Highest SSR record is now: " + str(high_SSR))
            elif option.upper() == "EXIT":
                print("Thank you for using Single Sound Recognition(SSR).")
                flag = 0
            elif option.upper() == "REPLAY":
                replay = 1
                print("")
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
            else:
                print("Your answer was incorrect. Resetting local SSR record.")
                print("The chord that was played was: " + arr[rand][:-4])
                local_SSR = 0

## function objective: provide functionality for training program for Chord Progression Recognition ##
def CPR():

    ## instantiate variables ##
    arr = os.listdir('Chords')
    flag = 1
    local_CPR = 0
    global high_CPR

    ## main function loop ##
    while(flag == 1):
        replay = 1
        print("\n\n-----CHORD PROGRESSION RECOGNITION(CPR)-----")
        print("\nYour highest CPR record is: " + str(high_CPR))
        print("Your current CPR record is: " + str(local_CPR))
        print("List of possible chords:")
        for chord in arr:
            print("  " + chord[:-4])
        rand1 = random.randint(0, len(arr) - 1)
        rand2 = random.randint(0, len(arr) - 1)
        rand3 = random.randint(0, len(arr) - 1)
        rand4 = random.randint(0, len(arr) - 1)
        mixer.init()
        mixer.music.load('Chords/'+arr[rand1])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)
        mixer.music.load('Chords/'+arr[rand2])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)
        mixer.music.load('Chords/'+arr[rand3])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)
        mixer.music.load('Chords/'+arr[rand4])
        for i in range(3):
            mixer.music.play()
            time.sleep(2)
            
        ## option processing ##
        while(replay == 1):
            replay = 0
            print("Please answer using the following format: i.e. Aminor, Emajor, Eminor, Dmajor")
            option = input("Which chords were played? (To exit, type 'EXIT', to replay, type 'REPLAY'): ")
            if option.upper() == arr[rand1][:-4].upper() + ", " + arr[rand2][:-4].upper() + ", " + arr[rand3][:-4].upper() + ", " + arr[rand4][:-4].upper():
                print("Congrats! You got it right!")
                local_CPR += 1
                if local_CPR > high_CPR:
                    high_CPR = local_CPR
                    print("CPR record Updated! Highest CPR record is now: " + str(high_CPR))
            elif option.upper() == "EXIT":
                print("Thank you for using Chord Progression Recognition(CPR).")
                flag = 0
            elif option.upper() == "REPLAY":
                replay = 1
                print("")
                mixer.music.load('Chords/'+arr[rand1])
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
                mixer.music.load('Chords/'+arr[rand2])
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
                mixer.music.load('Chords/'+arr[rand3])
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
                mixer.music.load('Chords/'+arr[rand4])
                for i in range(3):
                    mixer.music.play()
                    time.sleep(2)
            else:
                print("Your answer was incorrect. Resetting local CPR record.")
                print("The chord progression that was played was: " + arr[rand1][:-4] + ", " + arr[rand2][:-4] + ", " + arr[rand3][:-4] + ", " + arr[rand4][:-4])
                local_CPR = 0

## function objective: provide functionality for help Section ##  
def Help():
    flag = 1

    ## lists options that can be selected ##
    while(flag == 1):
        print("\n\n-----HELP SECTION-----")
        print("\nList of training options:")
        print("1. Chord Quality Recognition(CQR)")
        print("2. Single Sound Recognition(SSR)")
        print("3. Chord Progression Recognition(CPR)")
        print("4. Exit")

        ## processes user input through if-elif-else statements ##
        option = input("Which option would you like further information on?: ")
        if option == "1":
            print("Chord Quality Recognition(CQR)")
            print("A chord will be played and given to you in name only.")
            print("You will identify whether the chord was played in its Minor or Major variant.")
        elif option == "2":
            print("Single Sound Recognition(SSR)")
            print("A single chord will be played 3 times chosen at random.")
            print("You will be prompted for what chord was played.")
        elif option == "3":
            print("Chord Progression Recognition(CPR)")
            print("A series of 4 chords will be played consecutively, with each chord being played 3 times before the next.")
            print("You will be prompted to list the chords that were played in the correct order.")
        elif option == "4":
            print("Hope this helps, good luck in your training.")
            flag = 0
        else:
            print("You're selected option was not amongst those listed.")

## main function ##
def main():
    random.seed(datetime.now())
    intro()
    flag = 1
    while(flag == 1):
        options()
        option = input("Please select your option: ")
        if option == "1":
            CQR()
        elif option == "2":
            SSR()
        elif option == "3":
            CPR()
        elif option == "4":
            Help()
        elif option == "5":
            print("Thank you for training with us. Hope to you see again. <3")
            flag = 0
        else:
            print("Please select an option from the provided list.")
            
main()
    
